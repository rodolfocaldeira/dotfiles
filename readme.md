# My (Rodolfo's) dotfiles

## Install

To install simply do:

- `git clone git@bitbucket.org:rodolfocaldeira/dotfiles.git`
- `cd dotfiles`
- copy the `git/gitconfig.symlink.example` to `git/gitconfig.symlink` and add your details
- `sh install.sh` 

The install script will symlink the files to your home directory. That's it you're done.

## Thank you

I drew a lot of inspiration from the following projects, thanks for sharing :)

- [Mathias’s dotfiles](https://github.com/mathiasbynens/dotfiles)
- [Ryan Bates Dot Files](https://github.com/ryanb/dotfiles)
- [Holman does dotfiles](https://github.com/holman/dotfiles)